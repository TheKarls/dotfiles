return {
	"rebelot/kanagawa.nvim",
	priority = 1000,
	config = function()
		vim.cmd([[colorscheme kanagawa-wave]])
		vim.o.termguicolors = true
	end,
}
