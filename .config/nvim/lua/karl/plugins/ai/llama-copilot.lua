return {
	"Faywyn/llama-copilot.nvim",
	event = "InsertEnter",
	dependencies = {
		"nvim-lua/plenary.nvim",
	},
	config = function()
		local llama = require("llama-copilot")

		llama.setup({
			host = "10.0.0.123",
			port = "11434",
			model = "codellama:13b-code-q4_K_M",
			max_completion_size = 30, -- use -1 for limitless
			debug = false,
		})

		--vim.keymap.set("n", "<leader>ls", "<cmd>LlamaCopilotComplet<CR>", { desc = "Show code suggestion" }) -- show  diagnostics for file
		--vim.keymap.set("n", "<leader>la", "<cmd>LlamaCopilotAccept<CR>", { desc = "Accept code suggestion" }) -- show  diagnostics for file
	end,
}
