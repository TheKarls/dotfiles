vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- vim.opt.backspace = '2'
vim.opt.showcmd = true
vim.opt.laststatus = 2
-- vim.opt.autowrite = true
vim.opt.cursorline = true
vim.opt.autoread = true

vim.o.incsearch = true
vim.o.hlsearch = true

-- use spaces for tabs and whatnot
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.o.softtabstop = 4
vim.opt.shiftround = true
vim.o.autoindent = true

-- vim.opt.expandtab = true

vim.cmd [[ set termguicolors ]]

-- uine numbers
vim.wo.number = true
vim.wo.relativenumber = true

-- Speed up scrolling in Vim
vim.o.ttyfast = true

-- Copy / Paste
vim.opt.clipboard = "unnamedplus"

vim.opt.splitright = true
vim.opt.splitbelow = true

-- turn off swapfile
vim.opt.swapfile = false

-- timeout for hint
vim.o.timeout = true
vim.o.timeoutlen = 500
