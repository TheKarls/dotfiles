vim.keymap.set('n', '<leader>h', '<cmd>nohlsearch<CR>', { desc = "Clear an active search" }) -- Clear an active search

-- move split panes to left/bottom/top/right
vim.keymap.set('n', '<A-h>', '<C-W>H', {})
vim.keymap.set('n', '<A-j>', '<C-W>J', {})
vim.keymap.set('n', '<A-k>', '<C-W>K', {})
vim.keymap.set('n', '<A-l>', '<C-W>L', {})

-- move between panes to left/bottom/top/right
vim.keymap.set('n', '<C-h>', '<C-w>h', {})
vim.keymap.set('n', '<C-j>', '<C-w>j', {})
vim.keymap.set('n', '<C-k>', '<C-w>k', {})
vim.keymap.set('n', '<C-l>', '<C-w>l', {})

-- disable arrow in normal mode
vim.keymap.set('n', '<Up>', ":echo 'Nope, Git Gud'<CR>", {})
vim.keymap.set('n', '<Down>', ":echo 'Nope, Git Gud'<CR>", {})
vim.keymap.set('n', '<Left>', ":echo 'Nope, Git Gud'<CR>", {})
vim.keymap.set('n', '<Right>', ":echo 'Nope, Git Gud'<CR>", {})

-- map <cr> to coc#pum#visible() ? coc#_select_confirm() : "\<C-g>u\<CR>"
-- vim.keymap.set('i', '<cr>', 'v:lua.coc_select_confirm()', {expr = true, noremap = true, silent = true})

-- window management
-- vim.keymap.set("n", "<leader>sv", "<C-w>v", { desc = "Split window vertically" }) -- split window vertically
-- vim.keymap.set("n", "<leader>sh", "<C-w>s", { desc = "Split window horizontally" }) -- split window horizontally
-- vim.keymap.set("n", "<leader>se", "<C-w>=", { desc = "Make splits equal size" }) -- make split windows equal width & height
-- vim.keymap.set("n", "<leader>sx", "<cmd>close<CR>", { desc = "Close current split" }) -- close current split window

-- vim.keymap.set("n", "<leader>to", "<cmd>tabnew<CR>", { desc = "Open new tab" }) -- open new tab
-- vim.keymap.set("n", "<leader>tx", "<cmd>tabclose<CR>", { desc = "Close current tab" }) -- close current tab
-- vim.keymap.set("n", "<leader>tn", "<cmd>tabn<CR>", { desc = "Go to next tab" }) --  go to next tab
-- vim.keymap.set("n", "<leader>tp", "<cmd>tabp<CR>", { desc = "Go to previous tab" }) --  go to previous tab
-- vim.keymap.set("n", "<leader>tf", "<cmd>tabnew %<CR>", { desc = "Open current buffer in new tab" }) --  move current buffer to new tab
