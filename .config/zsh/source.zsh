source_if_exists () {
    if test -r "$1"; then
        source "$1"
    fi
}

source_if_exists $HOME/.config/zsh/arcolinux-aliases.zsh
source_if_exists $HOME/.config/zsh/aliases.zsh
source_if_exists $HOME/.config/zsh/export.zsh
source_if_exists $HOME/.config/zsh/fzf.zsh
source_if_exists $HOME/.config/zsh/tools.zsh

